﻿using System;
using System.Collections.Generic;

namespace Task6YellowPagesUpgrade
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            List<Person> listOfPeople = new List<Person>();

            Person p1 = new Person("Irfan5", "Nazand5", "45572847");
            Person p2 = new Person("Rober", "Lewandowski", "4556847");
            Person p3 = new Person("Manual", "Neuer", "45573547");
            Person p4 = new Person("Irfan2", "Nazand2", "78572847");
            Person p5 = new Person("Irfan1", "Nazand1", "98572847");

            listOfPeople.Add(p1);
            listOfPeople.Add(p2);
            listOfPeople.Add(p3);
            listOfPeople.Add(p4);
            listOfPeople.Add(p5);


            //SearchContact(firstName, lastName, listOfPeople);

            Person.SearchContact("Irfan1", "Nazand1", listOfPeople);
          
            
        }
    }
}
