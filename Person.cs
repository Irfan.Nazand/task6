﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task6YellowPagesUpgrade
{
    class Person
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string tlfNumber { get; set; }

        public Person() { }

        public Person(string FirstName, string LastName, string TlfNumber)
        {
            firstName = FirstName;
            lastName = LastName;
            tlfNumber = TlfNumber;
        }

        #region
        public static void myContacts()
        {
         
            Console.Write("Who are you searching for? Please enter firstname: ");
            string firstName = Console.ReadLine();

            Console.Write("Please enter their lastname: ");
            string lastName = Console.ReadLine();

          //  SearchContact(firstName, lastName);
            
        }

        /**
         * Search contact method
         * Checks user input, and if input is valid and contact exist, returns the contact from the dictionary to console.
         * If user does not exist, returns message to user with information that user does not exist.
         **/
        public static void SearchContact(string firstName, string lastName, List<Person> li)
        {
            double noOneFound = 0;

            foreach (var person in li )
            {
                if (person.firstName.ToLower().Equals(firstName.ToLower()) && person.lastName.ToLower().Equals(lastName.ToLower()))
                {
                    Console.WriteLine("There was a person in our contacts: " + person.firstName + " " + person.lastName + " " + person.tlfNumber);
                }

                else if (person.firstName.ToLower().Contains(firstName.ToLower()))
                {
                    Console.WriteLine("There was a partial match for the Firstname you are searching for: " + person.firstName + " " + person.lastName + " " + person.tlfNumber);
                }

                else if (person.lastName.ToLower().Contains(lastName.ToLower()))
                {
                    Console.WriteLine("There was a partial match for the lastname you are searching for: " + person.firstName + " " + person.lastName + " " + person.tlfNumber);
                }
                
                else
                {
                    noOneFound += 1;
                }
            }

            if ((li.Count / noOneFound) == 1)
            {
                Console.WriteLine("There was no matches with your search of firstname and lastname: " + firstName + " " + lastName);
            }
        }
        #endregion
    }
}
